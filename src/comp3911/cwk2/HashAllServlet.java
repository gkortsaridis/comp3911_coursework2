package comp3911.cwk2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.*;
import java.util.ArrayList;

import static comp3911.cwk2.Hasher.createHash;
import static comp3911.cwk2.Hasher.toHex;

public class HashAllServlet extends HttpServlet {

  private static final String CONNECTION_URL = "jdbc:sqlite:db.sqlite3";
  private static final String USERS_QUERY = "select * from user";
  private static final String UPDATE_QUERY = "UPDATE user SET username=\"%s\", password=\"%s\" WHERE id=%s";
  private Connection database;


  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    connectToDatabase();

    resp.setContentType("text/html");
    resp.setStatus(HttpServletResponse.SC_OK);
    if(hashAll()){
      resp.getWriter().println("<h1>Everything Hashed! :) </h1>");
    }else{
      resp.getWriter().println("<h1>We encountered an error! :( </h1>");
    }
  }

  private void connectToDatabase() throws ServletException {
    try {
      database = DriverManager.getConnection(CONNECTION_URL);
    }
    catch (SQLException error) {
      throw new ServletException(error.getMessage());
    }
  }

  private boolean hashAll() {
    ArrayList<String> hashedUsernames = new ArrayList<>();
    ArrayList<String> hashedPasswords = new ArrayList<>();
    ArrayList<String> ids = new ArrayList<>();
    Statement stmt = null;
    try {
      stmt = database.createStatement();
      ResultSet results = stmt.executeQuery(USERS_QUERY);
      while (results.next()) {
        String id = results.getString(1);
        String username = results.getString(3);
        String password = results.getString(4);

        String hashedUsername = toHex(createHash(username, password));
        String hashedPassword = toHex(createHash(password, username));

        ids.add(id);
        hashedPasswords.add(hashedPassword);
        hashedUsernames.add(hashedUsername);
      }

      return updateAll(hashedUsernames, hashedPasswords, ids);

    } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException | ServletException error) {
      System.out.println(error.toString());
      return false;
    }

  }

  private boolean updateAll(ArrayList<String> hashedUsernames, ArrayList<String> hashedPasswords, ArrayList<String> ids) throws SQLException, ServletException {
    Statement stmt = database.createStatement();
    for(int i=0; i< hashedPasswords.size(); i++){
      String query = String.format(UPDATE_QUERY, hashedUsernames.get(i), hashedPasswords.get(i), ids.get(i));
      System.out.println(query);
      stmt.addBatch(query);
    }
    stmt.executeBatch();

    return true;
  }
}
