package comp3911.cwk2;

import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.ssl.SslContextFactory;

public class AppServer {
  public static void main( String[] args ) throws Exception {
    ServletHandler handler = new ServletHandler();
    handler.addServletWithMapping(AppServlet.class, "/*");
    handler.addServletWithMapping(HashAllServlet.class, "/hashAll");

    Server server = new Server();

    //Creating HTTP connection on 8080. If we want to force http only
    //we can delete the connector object and not set it on server later
    ServerConnector connector = new ServerConnector(server);
    connector.setPort(8080);

    //Rest is for creation of HTTPS connection on port 9999
    HttpConfiguration https = new HttpConfiguration();
    https.addCustomizer(new SecureRequestCustomizer());
    SslContextFactory sslContextFactory = new SslContextFactory();
    String keystoreFile = AppServer.class.getResource("/keystore.jks").toExternalForm();
    if(keystoreFile != null){
      System.out.println(keystoreFile);
      sslContextFactory.setKeyStorePath(keystoreFile);
      sslContextFactory.setKeyStorePassword("123456");
      sslContextFactory.setKeyManagerPassword("123456");

      ServerConnector sslConnector = new ServerConnector(server,
          new SslConnectionFactory(sslContextFactory, "http/1.1"),
          new HttpConnectionFactory(https));
      sslConnector.setPort(9999);

      server.setConnectors(new Connector[] { connector, sslConnector });

      server.setHandler(handler);
      server.start();
      server.join();
    }else{
      System.out.println("NULL KEYSTORE FILE");
    }


  }
}
